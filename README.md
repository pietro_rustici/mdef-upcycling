# MDEF - Challenge 1 (15/02/2020 - 19/02/2020) - (Team 7) 
### David Wyss - [personal webpage](https://david_wyss.gitlab.io/mdef-class/) <br> Pietro Rustici - [personal webpage](https://pietro_rustici.gitlab.io/mdef-website/)

##### Rubric
1. Initial idea/Concept of the Project                                          **(done)**
2. Propose (What is supposed to do or not to do)                                **(done)**
3. Shown how your team planned and executed the project                         **(done)**
4. System diagram (illustration explaining function, parts, and relations)      **(done)**
5. Design elements (How you designed it)                                        **(done)**
6. How did you fabricate it (fabrication processes and materials)               **(done)**
7. Design & Fabrication files                                                   **(done)**
8. BOM (Build of Materials)                                                     **(done-documents-excel)**
9. The coding Logic (Algorithms and flowcharts, pseudocoding )                  **(done-none)**
10. Photographies of the end artifacts.                                         **(done)**
11. Iteration Process - Described problems and how the team solved them         **(done)**
12. Listed future development opportunities for this project                    **(done)**
13. Linked to your individual pages                                             **(done)**

---
## PERCULID – SMART • TRANSPARENT • HONEST
*Perculidus // An extensive cloud patch, sheet or layer, with distinct but sometimes very small spaces between
the elements*<br>

Driven by the growing call for transparency, trust and social identity. PerculID
reveals the ‘real’ value behind a garment. It allows to create and buy more
consciously – for the good of everyone, the environment, individuals, and
communities. Powered by technology and the commitment to open knowledge.
Easily accessible for you, and everyone. A platform for fashion of tomorrow.
Accessible for everyone, not only the big brands in the market.<br>

We believe in revealing more about a product, its origin and history. With our
platform, we allow to reveal information and stories about who made it, who has
worn it and also allow each wearer to give it its own emotional value. Whether by
adding your own personal pictures, or adding an individual love letter to your
garment, each wearer is able to provide the value of a textile. A playful experience,
revealing more information about second-hand clothing. Connecting to shoppers’
changing values and help build a better world.<br>

Through this project, we are trying to explore the future of second-hand fashion,
and find out more about the importance of emotional value and personal identity
in the industry.<br>

<img style="margin-top: 30px;" src="images/presentation/screen_5.png">

Why? Because…
1. there is an ever-growing demand for transparency;
1. we’re moving into an area of emotional story-telling;


### 1. Ideation:

##### Goal of the Micro Challenge
Design and make “something” that can help the process of designing/creating/thinking/collaborating with others.<br>

We started by exploring different ideas straight from the beginning. As the topic was
centred around textile, we tried to come up with a couple of ideas in the time we had.<br>
One idea that caught our attention was an artifact that would make garment
information more accessible for any individual.

<img style="margin-top: 30px;" src="images/presentation/screen_1.png">

##### The Project Idea
We seek to create a smart garment tracker that is not only easily accessible and
scalable, but can also be used for second hand and upcycled clothing. Making not
only transparency in fashion more accessible and cheaper, but also allowing
wearers to spontaneously add emotional and personal stories related to the clothes
worn. A playful experience like this could provide more information about pre-used
clothes as well as add an emotional value to every piece worn. Through the project,
we are trying to explore the future of second-hand fashion, and the importance of
emotional value and personal identity.

<img style="margin-top: 30px;" src="images/presentation/screen_6.png">

To do so, we try to work across the following areas of knowledge:
1. Principles: GIT, HTML, Documentation
2. CAD: 2D Design, Rhino
3. Computer-controlled Cutting: Vinyl, Laser

### 2. Planning
We split up and worked across 3 different workstreams:

<img style="margin-top: 30px;" src="images/presentation/screen_7.png">

Based on the workstreams, we started planning our days for this weekly challenge.
At the beginning of the challenge, we also connected and engaged with a third
party (Francesco M.) to learn more about the interests and needs of the
industry.

<img style="margin-top: 30px;" src="images/presentation/screen_3.png">

### 3. Development & Exploration

For the development of the project, we had to include and think about three different
areas that needed to be developed simultaneously. Across input, process, and the
actual output of our artifact, we had to think about all potential options we could
work with – including the factor time.

<img style="margin-top: 30px;" src="images/presentation/screen_2.png">

<img style="margin-top: 30px;" src="images/presentation/screen_8.png">

##### Input
Due to the factor time, we decided to focus on exploring the world of QR codes, and
use them as the key input for our current artifact. The main idea was to prototype
with different QR codes, sizes and types – across different, pre-used textile materials.
Hereby, we decided to test the usage and the readability across the following
computer-controlled machines: Laser Cut, Vinyl Cut, 3D. Also, we wanted to identify
what would be the minimal size in terms of its readability.

##### Design Elements

<img style="margin-top: 30px;" src="images/presentation/screen_9.png">

The design of the main element was not super tricky. We basically exported the
QR code directly from QR-Code Generator. There were different types of QR
codes, so we first had to test and evaluate these types based on the readability
across different sizes.

<img style="margin-top: 30px;" src="images/presentation/screen_10.png">

To properly document all our samples and make the results accessible for others in
the Fab Lab, we created label tags including all essential information. We printed the
labels on black carton to increase its visibility.

<img style="margin-top: 30px;" src="images/presentation/screen_11.png">

##### Fabrication Process

The fabrication process was comprehensive research and exploration. The idea
was to identify the measurements and settings across as many different textile
materials as possible. We summarized all information around the ideal settings in
the Excel attached.

<img style="margin-top: 30px;" src="images/presentation/screen_12.png">

##### Challenges

During the process of exploring across different materials and with the help of various techniques, we’ve came across a couple of challenges.

Readability of QR Codes on Different Materials:<br>
There was a lot of exploration needed to find out the ideal settings for each material. Depending on power & speed, we were able to achieve different results – which is documented in-detail in our Excel Sheet. Even we found the ideal settings across materials, the readability of QR codes differ and are not always fully stable. For some of the materials, eg. woll, it was not possible to scan and detect clearly the QR code. So, further exploration and research with different technologies (e.g. NFC, Smart Thread) could help avoid these challenges and complement initial findings.   

QR Codes Across different Technologies:<br>
With the 3D Printer and Laser Cutter, we were not able to achieve the desired end results. Neither of them could be printed properly, nor be scanned with a mobile phone. For the initial exploration with 3D, we have played around with a size that was most probably still too small for the high number of details needed for a QR code. So, there is an additional opportunity to explore with different sizes and a higher number of layers. For the Vinyl Cutter on the other hand, it was impossible to get to the detail needed to scan a QR code properly. As we can clearly see on the picture below, most of the information needed could not be properly transformed on a sticker – without lots of manual hand work required. To avoid this struggle, it makes more sense to print the QR code directly on sticker paper. 

<img style="margin-top: 30px;" src="images/presentation/vinyl.jpg">

<br><br>

##### Photography
<img style="margin-top: 30px;" src="images/presentation/screen_13.jpg">

<img style="margin-top: 30px;" src="images/presentation/platform1.jpg">

<img style="margin-top: 30px;" src="images/presentation/platform2.jpg">

<img style="margin-top: 30px;" src="images/presentation/platform3.jpg">


##### Videos (link)
1. [laser etching 1](images/presentation/movie1.MP4)
2. [laser etching 1](images/presentation/movie2.MP4)
3. [laser etching 1](images/presentation/movie3.MP4)


##### Fabrication Files
1. Branding
        1. Logo + UX
2. QR Code
        2. Rhino File
        3. Label Tag
3. Output
        1. Programming Code

### 3. Reflection & Future Work

### 5. Next Ideas
1. Machine to Print QR Codes, NFC Tags
2. Explore with Smart Garment – Add Factors such as Emotion to Garment
